<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\News;
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }

    /**
     * @Route("/create", name="homepage")
     */
    public function createAction()
    {
        $news = new News();
        $news->setTitle('A Foo Bar');
        $news->setSort(100);
        $news->setText('Lorem ipsum dolor');

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($news);
        $em->flush();

        return new Response('Created news id '.$news->getId());
    }

    /**
     * @Route("/show/<id>")
     */
    public function showAction($id)
    {
        $news = $this->getDoctrine()
            ->getRepository('AppBundle:News')
            ->find($id);

        if (!$news) {
            throw $this->createNotFoundException('No news found for id '.$id);
        }

        var_dump($news);
    }
}
