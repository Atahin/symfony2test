<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use AppBundle\Entity\News;

class NewsController extends Controller
{

    public function showAction($id)
    {
        $news = $this->getDoctrine()
            ->getRepository('AppBundle:News')
            ->find($id);

        if (!$news) {
            throw $this->createNotFoundException('No news found for id '.$id);
        }
        $serializer = $this->container->get('serializer');
        $response = $serializer->serialize($news, 'json');
        return new Response($response);
    }

    public function listAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:News');
        $request = Request::createFromGlobals();
        $page = $request->query->get('page', 1);
        $pageSize = $request->query->get('pageSize', 2);
        $sort = $request->query->get('sort', 'sort');

        $query = $repository->createQueryBuilder('news')
            ->setMaxResults($pageSize)
            ->setFirstResult($pageSize*($page-1))
            ->orderBy('news.'.$sort, 'ASC')
            ->getQuery();

        $response = $query->getResult();

        $serializer = $this->container->get('serializer');
        $result = $serializer->serialize($response, 'json');
        return new Response($result);
    }
}
